#include "mapnavigationwindow.h"
#include "mapnavigationwindowplugin.h"

#if QT_VERSION < 0x050000
#include <QtCore/QtPlugin>
#endif

MapNavigationWindowPlugin::MapNavigationWindowPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void MapNavigationWindowPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool MapNavigationWindowPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *MapNavigationWindowPlugin::createWidget(QWidget *parent)
{
    return new MapNavigationWindow(parent);
}

QString MapNavigationWindowPlugin::name() const
{
    return QLatin1String("MapNavigationWindow");
}

QString MapNavigationWindowPlugin::group() const
{
    return QLatin1String("TixRealmClient");
}

QIcon MapNavigationWindowPlugin::icon() const
{
    return QIcon();
}

QString MapNavigationWindowPlugin::toolTip() const
{
    return QLatin1String("");
}

QString MapNavigationWindowPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool MapNavigationWindowPlugin::isContainer() const
{
    return false;
}

QString MapNavigationWindowPlugin::domXml() const
{
    return "<ui language=\"c++\">\n"
                " <widget class=\"MapNavigationWindow\" name=\"mapNavigationWindow\">\n"
                "  <property name=\"geometry\">\n"
                "   <rect>\n"
                "    <x>0</x>\n"
                "    <y>0</y>\n"
                "    <width>224</width>\n"
                "    <height>224</height>\n"
                "   </rect>\n"
                "  </property>\n"
                " </widget>\n"
                "</ui>\n";
}

QString MapNavigationWindowPlugin::includeFile() const
{
    return QLatin1String("mapnavigationwindow.h");
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(mapnavigationwindowplugin, MapNavigationWindowPlugin)
#endif


