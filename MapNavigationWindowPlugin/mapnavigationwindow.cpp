#include "mapnavigationwindow.h"
#include "ui_mapnavigationwindow.h"
#include <QtCore/qmath.h>
#include <QtGui/QMouseEvent>

#if QT_VERSION < 0x050000
#include <QtGui/QScrollBar>
#else
#include <QtWidgets/QScrollBar>
#endif

MapNavigationWindow::MapNavigationWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MapNavigationWindow)
{
    ui->setupUi(this);
    ui->frame->installEventFilter(this);
    ui->frame->setCursor(Qt::OpenHandCursor);

    graphicsView = 0;

    m_overViewRatio = 5.;
    scale = 1.;
    zoomIndex = -1;
}

MapNavigationWindow::~MapNavigationWindow()
{
    delete ui;
}

qreal MapNavigationWindow::overViewRatio() const
{
    return m_overViewRatio;
}

void MapNavigationWindow::setOverViewRatio(qreal newRatio)
{
    m_overViewRatio = newRatio;

    if(graphicsView)
    {
        QSizeF sceneOverViewSize = graphicsView->scene()->sceneRect().size();//tamańo real de la escena
        sceneOverViewSize.scale(QSizeF(graphicsView->size()) / m_overViewRatio, Qt::KeepAspectRatio);

        /**
         * d es la dimensión del widget de visualización de la miniatura, calculada para
         * que la miniatura quepa completamente en cualquier dirección que sea rotada.
         */
        int d = qSqrt(qPow(sceneOverViewSize.width(), 2.) + qPow(sceneOverViewSize.height(), 2.));
        setMinimumSize(d, d);
        setMaximumSize(d, d);

        targetRect = QRectF(QPointF((d-sceneOverViewSize.width())/2., (d-sceneOverViewSize.height())/2.), sceneOverViewSize);
    }
}

void MapNavigationWindow::setGraphicsView(QGraphicsView *gView)
{
    graphicsView = gView;
    angle = old_angle = 0;

    QSizeF sceneOverViewSize = graphicsView->scene()->sceneRect().size();//tamańo real de la escena

    sceneOverViewSize.scale(QSizeF(graphicsView->size()) / m_overViewRatio, Qt::KeepAspectRatio);

    /**
     * overViewSize es la dimensión del widget de visualización de la miniatura, calculada para
     * que la miniatura quepa completamente en cualquier dirección que sea rotada.
     */
    int overViewWidgetSize = qSqrt(qPow(sceneOverViewSize.width(), 2.) + qPow(sceneOverViewSize.height(), 2.));
    setMinimumSize(overViewWidgetSize, overViewWidgetSize);
    setMaximumSize(overViewWidgetSize, overViewWidgetSize);

    bounds = targetRect = QRectF(QPointF((overViewWidgetSize-sceneOverViewSize.width())/2., (overViewWidgetSize-sceneOverViewSize.height())/2.), sceneOverViewSize);

    ui->frame->setGeometry(targetRect.toRect());

    connect(graphicsView->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(updateHorFramePos(int)), Qt::UniqueConnection);
    connect(graphicsView->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(updateVerFramePos(int)), Qt::UniqueConnection);
}

void MapNavigationWindow::rotate(int a)
{
    angle = a;

    //actualizar los límites de movimiento de la ventana de navegación
    QTransform transf;
    transf.translate(minimumWidth()/2., minimumHeight()/2.);
    transf.rotate(angle);
    transf.translate(-minimumWidth()/2., -minimumHeight()/2.);

    bounds = transf.mapRect(targetRect);

    transf.reset();
    transf.translate(minimumWidth()/2., minimumHeight()/2.);
    transf.rotate(angle - old_angle);
    transf.translate(-minimumWidth()/2., -minimumHeight()/2.);

    QPointF pr = ui->frame->geometry().center();
    pr = transf.map(pr);
    QPoint p = (pr - ui->frame->rect().center()).toPoint();

    //actualizar, de ser necesario, la posición de la ventana de navegación
    if(p.x() < bounds.left())
        p.setX(bounds.left());

    if(p.y() < bounds.top())
        p.setY(bounds.top());

    if(p.x() + ui->frame->width() > bounds.right())
        p.setX(bounds.right() - ui->frame->width());

    if(p.y() + ui->frame->height() > bounds.bottom())
        p.setY(bounds.bottom() - ui->frame->height());

    ui->frame->move(p);

    old_angle = angle;

    update();
}

void MapNavigationWindow::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    if(!graphicsView)
        return;

    QPainter painter(this);
    painter.translate(minimumWidth()/2., minimumHeight()/2.);
    painter.rotate(angle);
    painter.translate(-minimumWidth()/2., -minimumHeight()/2.);

    if(cachedImg.isNull())
    {
        cachedImg = QImage(targetRect.size().toSize(), QImage::Format_ARGB32);
        cachedImg.fill(0);
        QPainter painter(&cachedImg);
        graphicsView->scene()->render(&painter);
    }

    painter.drawImage(targetRect, cachedImg);
}

void MapNavigationWindow::resizeOverViewFrame(int index)
{
    index += minimumZoomIndex;//!< desplazar el intervalo de zoom al origen, minScale = 0

    if(zoomIndex == index)
        return;

    zoomIndex = index;
    scale = qPow(qreal(2), -zoomIndex / qreal(50));

    QPointF pr = ui->frame->pos() + ui->frame->rect().center();

    ui->frame->resize((targetRect.size() * scale).toSize());

    //mantener invariable la posición del centro de la ventana
    ui->frame->move((pr - QPointF(ui->frame->width()/2., ui->frame->height()/2.)).toPoint());
}

void MapNavigationWindow::updateHorFramePos(int scrollPos)
{
    QScrollBar *sb = static_cast<QScrollBar*>(sender());
    qreal Rx = scrollPos / qreal(sb->maximum() - sb->minimum());

    QPoint p(bounds.left() + (bounds.width() - ui->frame->width()) * Rx, ui->frame->y());

    //actualizar, de ser necesario, la posición de la ventana de navegación
    if(p.x() < bounds.left())
        p.setX(bounds.left());

    if(p.y() < bounds.top())
        p.setY(bounds.top());

    if(p.x() + ui->frame->width() > bounds.right())
        p.setX(bounds.right() - ui->frame->width());

    if(p.y() + ui->frame->height() > bounds.bottom())
        p.setY(bounds.bottom() - ui->frame->height());

    ui->frame->move(p);
}

void MapNavigationWindow::updateVerFramePos(int scrollPos)
{
    QScrollBar *sb = static_cast<QScrollBar*>(sender());
    qreal Ry = scrollPos / qreal(sb->maximum() - sb->minimum());

    QPoint p(ui->frame->x(), bounds.top() + (bounds.height() - ui->frame->height()) * Ry);

    //actualizar, de ser necesario, la posición de la ventana de navegación
    if(p.x() < bounds.left())
        p.setX(bounds.left());

    if(p.y() < bounds.top())
        p.setY(bounds.top());

    if(p.x() + ui->frame->width() > bounds.right())
        p.setX(bounds.right() - ui->frame->width());

    if(p.y() + ui->frame->height() > bounds.bottom())
        p.setY(bounds.bottom() - ui->frame->height());

    ui->frame->move(p);
}

bool MapNavigationWindow::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress)
    {
        if(obj == ui->frame)
        {            
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(me->button() == Qt::LeftButton)
            {
                ui->frame->setCursor(Qt::ClosedHandCursor);

                QObject::disconnect(graphicsView->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(updateHorFramePos(int)));
                QObject::disconnect(graphicsView->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(updateVerFramePos(int)));

                dragPosition = me->globalPos() - ui->frame->pos();
                me->accept();
                return true;
            }
        }
    }else

    if(event->type() == QEvent::MouseMove)
    {
        if(obj == ui->frame)
        {
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(me->buttons() & Qt::LeftButton)
            {
                QPoint p = me->globalPos() - dragPosition;

                if(p.x() < bounds.left())
                    p.setX(bounds.left());

                if(p.x() + ui->frame->width() > bounds.right())
                    p.setX(bounds.right() - ui->frame->width());

                if(p.y() < bounds.top())
                    p.setY(bounds.top());

                if(p.y() + ui->frame->height() > bounds.bottom())
                    p.setY(bounds.bottom() - ui->frame->height());

                ui->frame->move(p);

                qreal Rx = qreal(p.x() - bounds.left()) / (bounds.width() - ui->frame->width());
                qreal Ry = qreal(p.y() - bounds.top()) / (bounds.height() - ui->frame->height());

                int hsbMax = graphicsView->horizontalScrollBar()->maximum();
                int hsbMin = graphicsView->horizontalScrollBar()->minimum();

                int vsbMax = graphicsView->verticalScrollBar()->maximum();
                int vsbMin = graphicsView->verticalScrollBar()->minimum();

                graphicsView->horizontalScrollBar()->setValue(int(hsbMin + (hsbMax-hsbMin) * Rx));
                graphicsView->verticalScrollBar()->setValue(int(vsbMin + (vsbMax-vsbMin) * Ry));

                me->accept();
                return true;
            }
        }
    }else

    if(event->type() == QEvent::MouseButtonRelease)
    {
        if(obj == ui->frame)
        {
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(me->button() == Qt::LeftButton)
            {
                ui->frame->setCursor(Qt::OpenHandCursor);

                connect(graphicsView->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(updateHorFramePos(int)), Qt::UniqueConnection);
                connect(graphicsView->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(updateVerFramePos(int)), Qt::UniqueConnection);

                me->accept();
                return true;
            }
        }
    }

    return false;
}

/**
  * Sets the current centerpoint.  Also updates the scene's center point.
  * Unlike centerOn, which has no way of getting the floating point center
  * back, SetCenter() stores the center point.  It also handles the special
  * sidebar case.  This function will claim the centerPoint to sceneRec ie.
  * the centerPoint must be within the sceneRec.
  */
//Set the current centerpoint in the
void MapNavigationWindow::setCenterOfView(const QPointF& centerPoint) {
    //Get the rectangle of the visible area in scene coords
    QRectF visibleArea = graphicsView->mapToScene(graphicsView->rect()).boundingRect();

    //Get the scene area
    QRectF sceneBounds = graphicsView->sceneRect();

    double boundX = visibleArea.width() / 2.0;
    double boundY = visibleArea.height() / 2.0;
    double boundWidth = sceneBounds.width() - boundX;
    double boundHeight = sceneBounds.height() - boundY;

    //The max boundary that the centerPoint can be to
    QRectF bounds(boundX, boundY, boundWidth, boundHeight);

    if(bounds.contains(centerPoint)) {
        //We are within the bounds
        CurrentCenterPoint = centerPoint;
    } else {
        //We need to clamp or use the center of the screen
        if(visibleArea.contains(sceneBounds)) {
            //Use the center of scene ie. we can see the whole scene
            CurrentCenterPoint = sceneBounds.center();
        } else {

            CurrentCenterPoint = centerPoint;

            //We need to clamp the center. The centerPoint is too large
            if(centerPoint.x() > bounds.x() + bounds.width()) {
                CurrentCenterPoint.setX(bounds.x() + bounds.width());
            } else if(centerPoint.x() < bounds.x()) {
                CurrentCenterPoint.setX(bounds.x());
            }

            if(centerPoint.y() > bounds.y() + bounds.height()) {
                CurrentCenterPoint.setY(bounds.y() + bounds.height());
            } else if(centerPoint.y() < bounds.y()) {
                CurrentCenterPoint.setY(bounds.y());
            }

        }
    }

    //Update the scrollbars
    graphicsView->centerOn(CurrentCenterPoint);
    //graphicsView->ensureVisible(QRectF(CurrentCenterPoint-QPointF(10,10), CurrentCenterPoint+QPointF(10,10)));
}


