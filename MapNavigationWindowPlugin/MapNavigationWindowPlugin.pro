TARGET      = $$qtLibraryTarget(mapnavigationwindowplugin)

QT      +=  widgets designer
INCLUDEPATH += $$(INCLUDEPATHQt5)

TEMPLATE    = lib

CONFIG      += plugin c++11
#CONFIG-=embed_manifest_dll

#DEFINES += BUILDING_MAPNAVIGATIONWINDOW=1

#target.path = $$(QT_INSTALL_PLUGINS)/designer
#INSTALLS    += target

#DESTDIR += $$(QTDIR)/designer

CONFIG(debug, debug|release) {
win32 {
    QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\MapNavigationWindowPlugin\debug\mapnavigationwindowplugind.dll ..\..\TixRealmClient-build-Desktop\debug\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\MapNavigationWindowPlugin\debug\mapnavigationwindowplugind.dll $$(QTDIR)\plugins\designer\& copy /Y ..\..\TixRealmClientPlugins-build-Desktop\MapNavigationWindowPlugin\debug\mapnavigationwindowplugind.dll ..\..\TixRealmTests-build-Desktop\debug\designer\

    !win32-g++ {
        QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\MapNavigationWindowPlugin\debug\mapnavigationwindowplugind.pdb ..\..\TixRealmClient-build-Desktop\debug\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\MapNavigationWindowPlugin\debug\mapnavigationwindowplugind.pdb $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\MapNavigationWindowPlugin\debug\mapnavigationwindowplugind.pdb ..\..\TixRealmTests-build-Desktop\debug\designer\
    }
}
else {
   # TODO: Unices
}
}

CONFIG(release, debug|release) {
win32 {
    QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\MapNavigationWindowPlugin\release\mapnavigationwindowplugin.dll ..\..\TixRealmClient-build-Desktop\release\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\MapNavigationWindowPlugin\release\mapnavigationwindowplugin.dll $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\MapNavigationWindowPlugin\release\mapnavigationwindowplugin.dll ..\..\TixRealmTests-build-Desktop\release\designer\
}
else {
   # TODO: Unices
}
}

HEADERS += mapnavigationwindowplugin.h \
    mapnavigationwindow.h

SOURCES += mapnavigationwindowplugin.cpp \
    mapnavigationwindow.cpp

FORMS += mapnavigationwindow.ui
