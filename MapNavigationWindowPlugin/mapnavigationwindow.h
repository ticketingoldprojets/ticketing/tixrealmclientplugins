#ifndef MAPNAVIGATIONWINDOW_H
#define MAPNAVIGATIONWINDOW_H

#include <QtCore/QString>
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QtGui/QWidget>
#include <QtGui/QGraphicsView>
#else
#include <QtWidgets/QWidget>
#include <QtWidgets/QGraphicsView>
#endif

namespace Ui {
class MapNavigationWindow;
}

class MapNavigationWindow : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(qreal overViewSize READ overViewRatio WRITE setOverViewRatio)
    
public:
    explicit MapNavigationWindow(QWidget *parent = 0);
    ~MapNavigationWindow();

    qreal overViewRatio() const;
    void setOverViewRatio(qreal newRatio);

    virtual void setGraphicsView(QGraphicsView *gView);
    virtual void setMinimumZoomIndex(int value){minimumZoomIndex = -value;} //!< El objetivo de tomar el inverso es desplazar el intervalo al origen

    Q_SLOT void rotate(int a);
    Q_SLOT void resizeOverViewFrame(int index);
    
protected:
    void paintEvent(QPaintEvent *event);

private:
    Ui::MapNavigationWindow *ui;
    QGraphicsView *graphicsView;
    int angle;
    int old_angle;
    qreal scale;
    int zoomIndex;
    qreal m_overViewRatio;
    QPoint dragPosition;
    QRectF targetRect;
    QRectF bounds;//límites del desplazamiento admitido a la ventana de navegación
    int minimumZoomIndex;

    QPointF CurrentCenterPoint;

    bool eventFilter(QObject *obj, QEvent *event);

    //Set and Get the current centerpoint
    void setCenterOfView(const QPointF& centerPoint);
    QPointF getCenterOfView() { return CurrentCenterPoint; }

    QImage cachedImg;

    Q_SLOT void updateHorFramePos(int scrollPos);
    Q_SLOT void updateVerFramePos(int scrollPos);
};

#endif // MAPNAVIGATIONWINDOW_H


