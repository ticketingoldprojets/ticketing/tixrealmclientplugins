#ifndef ROTATIONWIDGET_H
#define ROTATIONWIDGET_H

#include <QtCore/QString>
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QtGui/QWidget>
#include <QtGui/QRadioButton>
#else
#include <QtWidgets/QWidget>
#include <QtWidgets/QRadioButton>
#endif

/*#if defined(BUILDING_ROTATIONWIDGET)
#  define ROTATIONWIDGET_EXPORT Q_DECL_EXPORT
#else
#  define ROTATIONWIDGET_EXPORT Q_DECL_IMPORT
#endif*/

namespace Ui {
class RotationWidget;
}

class /*ROTATIONWIDGET_EXPORT*/ RotationWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit RotationWidget(QWidget *parent = 0);
    ~RotationWidget();

    Q_SIGNAL void valueChanged(int angle);
    
protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent * event);

private:
    Ui::RotationWidget *ui;
    QPoint dragPosition;
    qreal radius;
    int angle;
    QRadioButton *knob;
    bool eventFilter(QObject *obj, QEvent *event);

    Q_SLOT void rotBtnClicked();
};

#endif // ROTATIONWIDGET_H
