#include "rotationwidget.h"
#include "ui_rotationwidget.h"
#include <QtGui/QPainter>
#include <QtGui/QMouseEvent>
#include <QtCore/qmath.h>

static const qreal Q_PI   = qreal(3.14159265358979323846);   // pi
static const qreal Q_2PI  = qreal(6.28318530717958647693);   // 2*pi
static const qreal Q_PI2  = qreal(1.57079632679489661923);   // pi/2

#define knobSize 16
#define knobSize_2 8

RotationWidget::RotationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RotationWidget)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Widget | Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground, true);

    radius = (width() - knobSize) / 2.;

    //Crear y dar la posici�n inicial al knob a 90 grados
    qreal angle = 3*Q_PI2;
    QPointF pf(radius * qCos(angle) + (double)width()/2.0, radius * qSin(angle) + height()/2.0);
    pf -= QPointF(knobSize_2, knobSize_2);
    knob = new QRadioButton(this);
    knob->setChecked(true);
    knob->installEventFilter(this);
    knob->setGeometry(QRect(pf.toPoint(), QSize(knobSize,knobSize)));

    connect(ui->toolButton, SIGNAL(clicked()), this, SLOT(rotBtnClicked()));
    connect(ui->toolButton_2, SIGNAL(clicked()), this, SLOT(rotBtnClicked()));
    connect(ui->toolButton_3, SIGNAL(clicked()), this, SLOT(rotBtnClicked()));
    connect(ui->toolButton_4, SIGNAL(clicked()), this, SLOT(rotBtnClicked()));
}

RotationWidget::~RotationWidget()
{
    delete ui;
}

void RotationWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QPainter painter(this);
    QPen pen(QColor(240, 253, 255));
    pen.setWidth(2);
    painter.setPen(pen);
    qreal d = knobSize_2;
    painter.setRenderHint(QPainter::Antialiasing);
    painter.drawEllipse(QRectF(rect()).adjusted(d,d,-d,-d));
}

void RotationWidget::resizeEvent(QResizeEvent *event)//garantizar que siempre sea cuadrado = que siempre sea un c�rculo la trayectoria
{
    QWidget::resizeEvent(event);
    if(width() == height())
        return;

    int min = qMin(width(), height());

    setGeometry(pos().x(), pos().y(), min, min);

    //ajustar el radio del c�rculo y la posici�n del knob
    radius = (width() - knobSize) / 2.;

    //Restablecer la posici�n inicial del knob a 90 grados
    qreal angle = 3*Q_PI2;
    QPointF pf(radius * qCos(angle) + (double)width()/2.0, radius * qSin(angle) + height()/2.0);
    pf -= QPointF(knobSize_2, knobSize_2);
    knob->move(pf.toPoint());
}

bool RotationWidget::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress)
    {
        if(obj == knob)
        {
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(me->button() == Qt::LeftButton)
            {
                dragPosition = me->globalPos() - knob->pos();
                me->accept();
                return true;
            }
        }
    }else

    if(event->type() == QEvent::MouseMove)
    {
        if(obj == knob)
        {
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(me->buttons() & Qt::LeftButton)
            {
                QPoint p = me->globalPos() - dragPosition;

                double yy = -(double)height()/2.0 + p.y();
                double xx = (double)p.x() - width()/2.0;
                double a = (xx || yy) ? qAtan2(yy, xx) : 0;
                if (a < -Q_PI2)
                    a = a + Q_2PI;

                QPointF pf(radius * qCos(a) + (double)width()/2.0, radius * qSin(a) + height()/2.0);
                pf -= QPointF(knobSize_2, knobSize_2);

                knob->move(pf.toPoint());

                angle = int((a - 3*Q_PI2)*180./Q_PI);

                if(knob->isChecked())
                    emit valueChanged(angle);

                me->accept();
                return true;
            }
        }
    }else

    if(event->type() == QEvent::MouseButtonDblClick)
    {
        if(obj == knob)
        {
            knob->setChecked(!knob->isChecked());
            event->accept();
            return true;
        }
    }else

    if(event->type() == QEvent::MouseButtonRelease)
    {
        if(obj == knob)
        {
            QMouseEvent *me = static_cast<QMouseEvent*>(event);

            if(!(me->buttons() & Qt::LeftButton))
            {
                if(!knob->isChecked())
                {
                    emit valueChanged(angle);

                    me->accept();
                    return true;
                }
            }
        }
    }

    return false;
}

void RotationWidget::rotBtnClicked()
{
    QToolButton *btn = static_cast<QToolButton*>(sender());
    qreal a = btn->accessibleDescription().toInt() * Q_PI / 180;

    QPointF pf(radius * qCos(a) + (double)width()/2.0, radius * qSin(a) + height()/2.0);
    pf -= QPointF(knobSize_2, knobSize_2);

    knob->move(pf.toPoint());

    angle = int((a - 3*Q_PI2)*180./Q_PI);

    emit valueChanged(angle);
}
