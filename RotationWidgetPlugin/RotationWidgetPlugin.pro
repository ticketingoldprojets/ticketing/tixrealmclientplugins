TARGET      = $$qtLibraryTarget(rotationwidgetplugin)

QT      +=  widgets designer
INCLUDEPATH += $$(INCLUDEPATHQt5)

TEMPLATE    = lib

CONFIG      += plugin c++11

HEADERS     = rotationwidgetplugin.h
SOURCES     = rotationwidgetplugin.cpp
RESOURCES   = \
    MyFiles.qrc
LIBS        += -L. 

CONFIG(debug, debug|release) {
win32 {
    QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\RotationWidgetPlugin\debug\rotationwidgetplugind.dll ..\..\TixRealmClient-build-Desktop\debug\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\RotationWidgetPlugin\debug\rotationwidgetplugind.dll $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\RotationWidgetPlugin\debug\rotationwidgetplugind.dll ..\..\TixRealmTests-build-Desktop\debug\designer\

    !win32-g++ {
        QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\RotationWidgetPlugin\debug\rotationwidgetplugind.pdb ..\..\TixRealmClient-build-Desktop\debug\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\RotationWidgetPlugin\debug\rotationwidgetplugind.pdb $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\RotationWidgetPlugin\debug\rotationwidgetplugind.pdb ..\..\TixRealmTests-build-Desktop\debug\designer\
    }
}
else {
   # TODO: Unices
}
}

CONFIG(release, debug|release) {
win32 {
   QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\RotationWidgetPlugin\release\rotationwidgetplugin.dll ..\..\TixRealmClient-build-Desktop\release\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\RotationWidgetPlugin\release\rotationwidgetplugin.dll $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\RotationWidgetPlugin\release\rotationwidgetplugin.dll ..\..\TixRealmTests-build-Desktop\release\designer\
}
else {
   # TODO: Unices
}
}

include(rotationwidget.pri)

