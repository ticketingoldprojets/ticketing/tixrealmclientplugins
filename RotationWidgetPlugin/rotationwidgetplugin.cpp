#include "rotationwidget.h"
#include "rotationwidgetplugin.h"

#if QT_VERSION < 0x050000
#include <QtCore/QtPlugin>
#endif

RotationWidgetPlugin::RotationWidgetPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void RotationWidgetPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool RotationWidgetPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *RotationWidgetPlugin::createWidget(QWidget *parent)
{
    return new RotationWidget(parent);
}

QString RotationWidgetPlugin::name() const
{
    return QLatin1String("RotationWidget");
}

QString RotationWidgetPlugin::group() const
{
    return QLatin1String("TixRealmClient");
}

QIcon RotationWidgetPlugin::icon() const
{
    return QIcon();
}

QString RotationWidgetPlugin::toolTip() const
{
    return QLatin1String("");
}

QString RotationWidgetPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool RotationWidgetPlugin::isContainer() const
{
    return false;
}

QString RotationWidgetPlugin::domXml() const
{
    //return QLatin1String("<ui language=\"c++\"><widget class=\"PictureFlow\" name=\"pictureFlow\">\n</widget>\n");
    return "<ui language=\"c++\">\n"
                " <widget class=\"RotationWidget\" name=\"rotationWidget\">\n"
                "  <property name=\"geometry\">\n"
                "   <rect>\n"
                "    <x>0</x>\n"
                "    <y>0</y>\n"
                "    <width>100</width>\n"
                "    <height>100</height>\n"
                "   </rect>\n"
                "  </property>\n"
                " </widget>\n"
                "</ui>\n";
}

QString RotationWidgetPlugin::includeFile() const
{
    return QLatin1String("rotationwidget.h");
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(rotationwidgetplugin, RotationWidgetPlugin)
#endif
