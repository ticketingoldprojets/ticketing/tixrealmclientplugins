#include "pictureflow.h"
#include "pictureflowplugin.h"

#if QT_VERSION < 0x050000
#include <QtCore/QtPlugin>
#endif

PictureFlowPlugin::PictureFlowPlugin(QObject *parent)
    : QObject(parent)
{
    m_initialized = false;
}

void PictureFlowPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool PictureFlowPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *PictureFlowPlugin::createWidget(QWidget *parent)
{
    return new PictureFlowView(parent);
}

QString PictureFlowPlugin::name() const
{
    return QLatin1String("PictureFlowView");
}

QString PictureFlowPlugin::group() const
{
    return QLatin1String("TixRealmClient");
}

QIcon PictureFlowPlugin::icon() const
{
    return QIcon();
}

QString PictureFlowPlugin::toolTip() const
{
    return QLatin1String("");
}

QString PictureFlowPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool PictureFlowPlugin::isContainer() const
{
    return false;
}

QString PictureFlowPlugin::domXml() const
{
    //return QLatin1String("<ui language=\"c++\"><widget class=\"PictureFlow\" name=\"pictureFlow\">\n</widget>\n");
    return "<ui language=\"c++\">\n"
                " <widget class=\"PictureFlowView\" name=\"pictureFlowView\">\n"
                "  <property name=\"geometry\">\n"
                "   <rect>\n"
                "    <x>0</x>\n"
                "    <y>0</y>\n"
                "    <width>900</width>\n"
                "    <height>200</height>\n"
                "   </rect>\n"
                "  </property>\n"
                " </widget>\n"
                "</ui>\n";
}

QString PictureFlowPlugin::includeFile() const
{
    return QLatin1String("pictureflow.h");
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(pictureflowplugin, PictureFlowPlugin);
#endif
