/****************************************************************************
**
** Copyright (C) 2011 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the ActiveQt framework of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
**
****************************************************************************/

/*
  ORIGINAL COPYRIGHT HEADER
  PictureFlow - animated image show widget
  http://pictureflow.googlecode.com

  Copyright (C) 2007 Ariya Hidayat (ariya@kde.org)

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef PICTUREFLOW_H
#define PICTUREFLOW_H

#include <QtGui/QStandardItemModel>

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
#include <QtGui/QAbstractItemView>
#else
#include <QtWidgets/QAbstractItemView>
#endif

#ifndef DEBUG_PICTUREFLOW
#include <QtUiPlugin/QDesignerExportWidget>
#endif


class PictureFlowPrivate;

/*!
  Class PictureFlow implements an image show widget with animation effect
  like Apple's CoverFlow (in iTunes and iPod). Images are arranged in form
  of slides, one main slide is shown at the center with few slides on
  the left and right sides of the center slide. When the next or previous
  slide is brought to the front, the whole slides flow to the right or
  the right with smooth animation effect; until the new slide is finally
  placed at the center.
 */
class PictureFlowView : public QAbstractItemView
{
Q_OBJECT

    Q_PROPERTY(QSize slideSize READ slideSize WRITE setSlideSize)
    Q_PROPERTY(int zoomFactor READ zoomFactor WRITE setZoomFactor)
    Q_PROPERTY(QColor backGroundColor READ backGroundColor WRITE setBackGroundColor)

    PictureFlowPrivate* d;

protected:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void resizeEvent(QResizeEvent* event);
    void timerEvent(QTimerEvent* event);
    bool isIndexHidden(const QModelIndex &index) const;
    QModelIndex moveCursor(QAbstractItemView::CursorAction cursorAction,
                         Qt::KeyboardModifiers modifiers);
    int horizontalOffset() const;
    int verticalOffset() const;
    /*
      Find the indices corresponding to the extent of the selection.
    */
    void setSelection(const QRect&, QItemSelectionModel::SelectionFlags command);
    QRegion visualRegionForSelection(const QItemSelection &selection) const;

    void dropEvent(QDropEvent *event);

protected slots:
    //void dataChanged(const QModelIndex &topLeft, const QModelIndex &bottomRight);
    void rowsInserted(const QModelIndex &parent, int start, int end);
    void rowsAboutToBeRemoved(const QModelIndex & parent, int start, int end);
    void currentChanged(const QModelIndex &current, const QModelIndex &previous);

public:
    /*!
    Creates a new PictureFlowView widget.
    */
    explicit PictureFlowView(QWidget* parent = 0);

    /*!
    Destroys the widget.
    */
    ~PictureFlowView();

    /*!
    Returns the total number of slides.
    */
    virtual int slideCount() const;

    /*!
    Returns the dimension of each slide (in pixels).
    */
    virtual QSize slideSize() const;

    /*!
    Sets the dimension of each slide (in pixels).
    */
    virtual void setSlideSize(QSize size);

    /*!
    Sets the zoom factor (in percent).
    */
    virtual void setZoomFactor(int zoom);

    /*!
    Returns the zoom factor (in percent).
    */
    virtual int zoomFactor() const;

    /*!
    Clears any caches held to free up memory
    */
    virtual void clearCaches();

    /*!
    Returns QImage of specified slide.
    This function will be called only whenever necessary, e.g. the 100th slide
    will not be retrived when only the first few slides are visible.
    */
    virtual QImage slide(int index) const;

    /*!
    Sets an image for specified slide. If the slide already exists,
    it will be replaced.
    */
    virtual void setSlide(int index, const QImage& image);

    virtual void setSlideCaption(int index, QString caption);

    /*!
    Sets a pixmap for specified slide. If the slide already exists,
    it will be replaced.
    */
    virtual void setSlide(int index, const QPixmap& pixmap);

    /*!
    Returns the index of slide currently shown in the middle of the viewport.
    */
    virtual int currentSlide() const;

    /*
      Returns the item that covers the coordinate given in the view.
    */
    virtual QModelIndex indexAt(const QPoint &point) const;

    /*
      Returns the position of the item in viewport coordinates.
    */
    virtual QRect visualRect(const QModelIndex &index) const;

    virtual void scrollTo(const QModelIndex &index, ScrollHint hint = EnsureVisible);

    virtual void setModel(QAbstractItemModel * model);

    virtual void emitClicked(const QModelIndex &index);

    virtual QColor backGroundColor() const;
    virtual void setBackGroundColor(QColor &c);

signals:
    void newDropEvent(QDropEvent *event);

public slots:
    void setRootIndex(const QModelIndex &index);

    /*!
    Sets slide to be shown in the middle of the viewport. No animation
    effect will be produced, unlike using showSlide.
    */
    virtual void setCurrentSlide(int index);

    /*!
    Rerender the widget. Normally this function will be automatically invoked
    whenever necessary, e.g. during the transition animation.
    */
    virtual void render();

    /*!
    Shows previous slide using animation effect.
    */
    virtual void showPrevious();

    /*!
    Shows next slide using animation effect.
    */
    virtual void showNext();

    /*!
    Go to specified slide using animation effect.
    */
    virtual void showSlide(int index);
};

#endif // PICTUREFLOW_H
