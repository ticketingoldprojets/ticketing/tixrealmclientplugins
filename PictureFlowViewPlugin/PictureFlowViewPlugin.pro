TARGET      = $$qtLibraryTarget(pictureflowplugin)

QT      +=  widgets designer
INCLUDEPATH += $$(INCLUDEPATHQt5)

TEMPLATE    = lib

CONFIG      += plugin c++11

CONFIG(debug, debug|release) {
win32 {
    QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\PictureFlowViewPlugin\debug\pictureflowplugind.dll ..\..\TixRealmClient-build-Desktop\debug\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\PictureFlowViewPlugin\debug\pictureflowplugind.dll $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\PictureFlowViewPlugin\debug\pictureflowplugind.dll ..\..\TixRealmTests-build-Desktop\debug\designer\

    !win32-g++ {
        QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\PictureFlowViewPlugin\debug\pictureflowplugind.pdb ..\..\TixRealmClient-build-Desktop\debug\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\PictureFlowViewPlugin\debug\pictureflowplugind.pdb $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\PictureFlowViewPlugin\debug\pictureflowplugind.pdb ..\..\TixRealmTests-build-Desktop\debug\designer\
    }
}
else {
   # TODO: Unices
}
}

CONFIG(release, debug|release) {
win32 {
   QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\PictureFlowViewPlugin\release\pictureflowplugin.dll ..\..\TixRealmClient-build-Desktop\release\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\PictureFlowViewPlugin\release\pictureflowplugin.dll $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\PictureFlowViewPlugin\release\pictureflowplugin.dll ..\..\TixRealmTests-build-Desktop\release\designer\
}
else {
   # TODO: Unices
}
}

HEADERS += pictureflow.h \
        pictureflowplugin.h
SOURCES += pictureflow.cpp \
        pictureflowplugin.cpp
