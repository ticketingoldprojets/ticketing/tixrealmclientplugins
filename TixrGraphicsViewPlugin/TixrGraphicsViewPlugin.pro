TARGET      = $$qtLibraryTarget(tixrgraphicsviewplugin)

QT      +=  widgets designer
INCLUDEPATH += $$(INCLUDEPATHQt5)

QT += opengl

TEMPLATE    = lib

CONFIG         += plugin c++11
#CONFIG-=embed_manifest_dll

HEADERS     = tixrgraphicsviewplugin.h
SOURCES     = tixrgraphicsviewplugin.cpp
RESOURCES   =
LIBS        += -L. 

CONFIG(debug, debug|release) {
win32 {
    QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\TixrGraphicsViewPlugin\debug\tixrgraphicsviewplugind.dll ..\..\TixRealmClient-build-Desktop\debug\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\TixrGraphicsViewPlugin\debug\tixrgraphicsviewplugind.dll $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\TixrGraphicsViewPlugin\debug\tixrgraphicsviewplugind.dll ..\..\TixRealmTests-build-Desktop\debug\designer\

    !win32-g++ {
        QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\TixrGraphicsViewPlugin\debug\tixrgraphicsviewplugind.pdb ..\..\TixRealmClient-build-Desktop\debug\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\TixrGraphicsViewPlugin\debug\tixrgraphicsviewplugind.pdb $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\TixrGraphicsViewPlugin\debug\tixrgraphicsviewplugind.pdb ..\..\TixRealmTests-build-Desktop\debug\designer\
    }
}
else {
   # TODO: Unices
}
}

CONFIG(release, debug|release) {
win32 {
   QMAKE_POST_LINK=copy /Y ..\..\TixRealmClientPlugins-build-Desktop\TixrGraphicsViewPlugin\release\tixrgraphicsviewplugin.dll ..\..\TixRealmClient-build-Desktop\release\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\TixrGraphicsViewPlugin\release\tixrgraphicsviewplugin.dll $$(QTDIR)\plugins\designer\ & copy /Y ..\..\TixRealmClientPlugins-build-Desktop\TixrGraphicsViewPlugin\release\tixrgraphicsviewplugin.dll ..\..\TixRealmTests-build-Desktop\release\designer\
}
else {
   # TODO: Unices
}
}

include(tixrgraphicsview.pri)
