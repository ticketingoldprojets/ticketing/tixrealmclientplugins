#include "tixrgraphicsview.h"
#include "tixrgraphicsviewplugin.h"

#if QT_VERSION < 0x050000
#include <QtCore/QtPlugin>
#endif

TixrGraphicsViewPlugin::TixrGraphicsViewPlugin(QObject *parent) : QObject(parent)
{
    m_initialized = false;
}

void TixrGraphicsViewPlugin::initialize(QDesignerFormEditorInterface * /* core */)
{
    if (m_initialized)
        return;

    // Add extension registrations, etc. here

    m_initialized = true;
}

bool TixrGraphicsViewPlugin::isInitialized() const
{
    return m_initialized;
}

QWidget *TixrGraphicsViewPlugin::createWidget(QWidget *parent)
{
    return new TixrGraphicsView(parent);
}

QString TixrGraphicsViewPlugin::name() const
{
    return QLatin1String("TixrGraphicsView");
}

QString TixrGraphicsViewPlugin::group() const
{
    return QLatin1String("TixRealmClient");
}

QIcon TixrGraphicsViewPlugin::icon() const
{
    return QIcon();
}

QString TixrGraphicsViewPlugin::toolTip() const
{
    return QLatin1String("");
}

QString TixrGraphicsViewPlugin::whatsThis() const
{
    return QLatin1String("");
}

bool TixrGraphicsViewPlugin::isContainer() const
{
    return false;
}

QString TixrGraphicsViewPlugin::domXml() const
{
    return "<ui language=\"c++\">\n"
                " <widget class=\"TixrGraphicsView\" name=\"tixrGraphicsView\">\n"
                "  <property name=\"geometry\">\n"
                "   <rect>\n"
                "    <x>0</x>\n"
                "    <y>0</y>\n"
                "    <width>224</width>\n"
                "    <height>224</height>\n"
                "   </rect>\n"
                "  </property>\n"
                " </widget>\n"
                "</ui>\n";
}

QString TixrGraphicsViewPlugin::includeFile() const
{
    return QLatin1String("tixrgraphicsview.h");
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(tixrgraphicsviewplugin, TixrGraphicsViewPlugin)
#endif
