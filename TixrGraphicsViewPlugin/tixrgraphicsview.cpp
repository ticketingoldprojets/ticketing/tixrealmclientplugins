#include "tixrgraphicsview.h"
#include <QtGui/QMouseEvent>

#if QT_VERSION < 0x050000
#include <QtGui/QScrollBar>
#else
#include <QtWidgets/QScrollBar>
#endif

#ifndef QT_NO_OPENGL
#include <QtOpenGL/QtOpenGL>
#endif

TixrGraphicsView::TixrGraphicsView(QWidget *parent) : QGraphicsView(parent)
{
#ifndef QT_NO_OPENGL
    setViewport(new QGLWidget(QGLFormat(QGL::SampleBuffers)));
#endif

    viewport()->setCursor(Qt::OpenHandCursor);
    viewport()->setAttribute(Qt::WA_AcceptTouchEvents);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
}

TixrGraphicsView::~TixrGraphicsView()
{
}

void TixrGraphicsView::mouseMoveEvent(QMouseEvent * event)
{
    if(event->buttons() & Qt::RightButton)
    {
        QPoint delta = event->globalPos() - dragPoint;
        dragPoint  = event->globalPos();

        horizontalScrollBar()->setValue(horizontalScrollBar()->value() - delta.x());
        verticalScrollBar()->setValue(verticalScrollBar()->value() - delta.y());
        return;
    }

    QGraphicsView::mouseMoveEvent(event);
}

void TixrGraphicsView::mousePressEvent(QMouseEvent * event)
{
    if(event->button() == Qt::RightButton)
    {
        viewport()->setCursor(Qt::ClosedHandCursor);
        dragPoint  = event->globalPos();
        return;
    }

    QGraphicsView::mousePressEvent(event);
}

void TixrGraphicsView::mouseReleaseEvent(QMouseEvent * event)
{
    if(event->button() == Qt::RightButton)
    {
        viewport()->setCursor(Qt::OpenHandCursor);
        dragPoint  = QPoint();
        return;
    }

    QGraphicsView::mouseReleaseEvent(event);
}
