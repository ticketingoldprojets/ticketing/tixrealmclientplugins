#ifndef TIXRGRAPHICSVIEW_H
#define TIXRGRAPHICSVIEW_H

#include <QtGlobal>
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QtGui/QGraphicsView>
#else
#include <QtWidgets/QGraphicsView>
#endif

class TixrGraphicsView : public QGraphicsView
{
    Q_OBJECT
    
public:
    explicit TixrGraphicsView(QWidget *parent = 0);
    ~TixrGraphicsView();

private:
    QPoint dragPoint;

    void mouseMoveEvent(QMouseEvent * event);
    void mousePressEvent(QMouseEvent * event);
    void mouseReleaseEvent(QMouseEvent * event);
};

#endif // TIXRGRAPHICSVIEW_H
