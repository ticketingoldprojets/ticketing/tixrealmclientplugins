#ifndef TIXRGRAPHICSVIEWPLUGIN_H
#define TIXRGRAPHICSVIEWPLUGIN_H

#include <QtUiPlugin/customwidget.h>

class TixrGraphicsViewPlugin : public QObject, public QDesignerCustomWidgetInterface
{
    Q_OBJECT
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "com.santrossoft.TixRealmClient.TixrGraphicsView" FILE "tixrgraphicsviewplugin.json")
#endif
    Q_INTERFACES(QDesignerCustomWidgetInterface)

public:
    explicit TixrGraphicsViewPlugin(QObject *parent = 0);

    bool isContainer() const;
    bool isInitialized() const;
    QIcon icon() const;
    QString domXml() const;
    QString group() const;
    QString includeFile() const;
    QString name() const;
    QString toolTip() const;
    QString whatsThis() const;
    QWidget *createWidget(QWidget *parent);
    void initialize(QDesignerFormEditorInterface *core);

private:
    bool m_initialized;
};

#endif // TIXRGRAPHICSVIEWPLUGIN_H
