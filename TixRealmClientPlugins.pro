TEMPLATE = subdirs

SUBDIRS += \
    MapNavigationWindowPlugin \
    PictureFlowViewPlugin \
    RotationWidgetPlugin \
    TixrGraphicsViewPlugin
